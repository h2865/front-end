const cookieparser = process.server ? require('cookieparser') : undefined

export const state = () => ({
  accessToken: null,
  loggedIn: false,
  user: null,
  auth: null,
})

export const mutations =
{
  setUser (state, user) {
    state.user = user;
  },

  setLoggedIn (state, data) {
    state.loggedIn = data
  },

  setAuth (state, data) {
    state.auth = data
  }
}

export const actions =
{
  async nuxtServerInit ({ commit }, { req, redirect })
  {
    try
    {
      if(req.headers.cookie)
      {
        const { accessToken } = cookieparser.parse(req.headers.cookie);

        if(accessToken)
        {
          const user = await this.$axios.$get(`auth/get-user`);
          commit('setUser', user);
          commit('setLoggedIn', true);
        }
        else
        {
          commit('setUser', null);
          commit('setLoggedIn', false);
          redirect('/')
        }
      }
    }
    catch (e) {
      console.error(e)
    }
  }
}
